using System.Collections.Generic;
using UnityEngine;

namespace ObjectPool
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] private List<GameObject> pooledObjects;
        [SerializeField] private List<GameObject> objectsToPool;
        [SerializeField] private Transform obstacleParent;

        
        private void Start()
        {
            pooledObjects = new List<GameObject>();
        
            GameObject tmp;
        
            for (int i = 0; i < objectsToPool.Count; i++)
            {
                tmp = Instantiate(objectsToPool[i], obstacleParent);
                tmp.SetActive(false);
                pooledObjects.Add(tmp);
            }
        }
        
        public GameObject GetPooledObject()
        {
            List<GameObject> unActiveObjects = new List<GameObject>();
            
            for (int i = 0; i < objectsToPool.Count; i++)
            {
                if (!pooledObjects[i].activeInHierarchy)
                {
                    unActiveObjects.Add(pooledObjects[i]);
                }
            }
            
            return unActiveObjects[Random.Range(0, unActiveObjects.Count)];
        }

        public void DisableAllObjects()
        {
            for (int i = 0; i < objectsToPool.Count; i++)
            {
                if (pooledObjects[i].activeInHierarchy)
                {
                    pooledObjects[i].SetActive(false);
                }
            }
        }
    }
}