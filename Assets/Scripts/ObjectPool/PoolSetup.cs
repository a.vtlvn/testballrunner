using UnityEngine;

namespace ObjectPool
{
    public class PoolSetup : MonoBehaviour
    {
        public static PoolSetup Instance;
        
        [SerializeField] private ObjectPool objectPool;
        [SerializeField] private Transform obstacleSpawnPoint;
        [SerializeField] private Vector3 spawnOffset = Vector3.zero;


        private void Awake() => Instance = this;
    
        public void DestroyObstacles(GameObject obstacle) => obstacle.SetActive(false);

        public void DisableAllObstacles() => objectPool.DisableAllObjects();

        public void IntatiateObstacle()
        {
            GameObject pooledGameObject = objectPool.GetPooledObject();
        
            if (pooledGameObject != null)
            {
                pooledGameObject.transform.position = obstacleSpawnPoint.position + spawnOffset;
                pooledGameObject.SetActive(true);
            }
        }
    }
}
