using GameCore.Services;
using GameCore.UI;
using UnityEngine;

namespace GameCore.Obstacles
{
    public class GameOverChecker : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("Game over");
                
                GameService.OnGameOver?.Invoke();
            }
        }
    }
}
