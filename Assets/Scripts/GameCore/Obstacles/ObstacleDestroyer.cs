using ObjectPool;
using UnityEngine;

namespace GameCore.Obstacles
{
    public class ObstacleDestroyer : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Obstacle"))
            {
                PoolSetup.Instance.DestroyObstacles(other.gameObject);
            }
        }
    }
}
