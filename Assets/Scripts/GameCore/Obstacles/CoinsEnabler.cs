using UnityEngine;

namespace GameCore.Obstacles
{
    public class CoinsEnabler : MonoBehaviour
    {
        private GameObject[] coins;

        
        private void OnEnable()
        {
            foreach(Transform child in transform)
            {
                if(child.CompareTag("Coin"))
                    child.gameObject.SetActive(true);
            }
        }
    }
}
