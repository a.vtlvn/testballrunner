using ObjectPool;
using UnityEngine;

namespace GameCore.Obstacles
{
    public class ObstacleSpawnChecker : MonoBehaviour
    { 
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Obstacle"))
            {
                PoolSetup.Instance.IntatiateObstacle();
            }
        }
    }
}
