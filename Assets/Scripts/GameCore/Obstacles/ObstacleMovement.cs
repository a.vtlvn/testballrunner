using GameCore.Services;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    private void Update()
    {
        transform.Translate(Vector3.back * (Time.deltaTime * GameService.Instance.ObstacleSpeed));        
    }
}
