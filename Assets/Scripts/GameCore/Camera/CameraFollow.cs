using UnityEngine;

namespace CameraControllers
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform cameraTarget = null;
        [SerializeField] private Vector3 cameraOffset = Vector3.zero;
        [SerializeField] private float lerpSpeed = 50;

        private void Start()
        {
            cameraOffset = transform.position - cameraTarget.position;
        }

        private void LateUpdate()
        {
            Vector3 newCameraPosition = new Vector3(transform.position.x, transform.position.y, cameraOffset.z + cameraTarget.position.z);
            transform.position = Vector3.Lerp(transform.position, newCameraPosition, lerpSpeed * Time.deltaTime);
        }
    }
}
