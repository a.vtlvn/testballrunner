using GameCore.Coin;
using UnityEngine;

namespace GameCore.SaveLoad
{
    public class SaveLoadService : MonoBehaviour
    {
        private const string SAVE_KEY = "BestScoreKey";
        
        
        private void OnEnable() => CoinsService.OnCoinsAdded += SaveBestScore;

        private void OnDisable() => CoinsService.OnCoinsAdded -= SaveBestScore;

        public int GetBestScore() => PlayerPrefs.GetInt(SAVE_KEY, 0);
        
        public void SaveBestScore(int score)
        {
            if (score > GetBestScore())
                PlayerPrefs.SetInt(SAVE_KEY, score);
        }
    }
}
