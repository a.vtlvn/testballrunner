using UnityEngine;

namespace GameCore.Coin
{
    public class CoinPicker : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("Take coin");
                
                CoinsService.OnAddCoins?.Invoke();
                
                gameObject.SetActive(false);
                
            }
        }
    }
}
