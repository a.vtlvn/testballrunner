using UnityEngine;

namespace GameCore.Coin
{
    public class CoinsRotator : MonoBehaviour
    {
        [SerializeField] private float speed;

        private void Update() => transform.RotateAround(Vector3.up, speed * Time.deltaTime);
    }
}
