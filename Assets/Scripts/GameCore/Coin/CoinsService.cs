using System;
using GameCore.SaveLoad;
using GameCore.Services;
using UnityEngine;

namespace GameCore.Coin
{
    public class CoinsService : MonoBehaviour
    {
        [SerializeField] private int currentCoins = 0;
        [SerializeField] private SaveLoadService saveLoadService;
        
        public static CoinsService Instance;
        public static Action OnAddCoins;
        public static Action <int> OnCoinsAdded;


        private void Awake() => Instance = this;

        private void OnEnable()
        {
            OnAddCoins += AddCoin;
            GameService.OnGameOver += ResetCoins;
        }

        private void OnDisable()
        {
            OnAddCoins -= AddCoin;
            GameService.OnGameOver -= ResetCoins;
        }

        public int GetBestScore() => saveLoadService.GetBestScore();

        private void AddCoin()
        {
            currentCoins++;
            OnCoinsAdded?.Invoke(currentCoins);
        }
        
        private void ResetCoins() => currentCoins = 0;
    }
}
