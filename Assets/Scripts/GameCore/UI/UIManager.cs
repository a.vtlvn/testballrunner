using System;
using System.Collections.Generic;
using System.Linq;
using GameCore.Services;
using UnityEngine;

namespace GameCore.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private List<Screen> screens;
        [SerializeField] private string startScreenName;
        [SerializeField] private string gameOverScreen;
        
        private void OnEnable() => GameService.OnGameOver += ShowGameOverScreen;
        
        private void OnDisable() => GameService.OnGameOver -= ShowGameOverScreen;

        private void Start() => Show(startScreenName);
        
        private void ShowGameOverScreen() => ShowScreen(gameOverScreen);
    
        private void ShowScreen(string screenName)
        {
            HideScreens();
            
            foreach (Screen screen in screens.Where(screen => screen.ScreenID.Equals(screenName)))
            {
                screen.Show();
                break;
            }
        }

        private void HideScreens()
        {
            foreach (Screen screen in screens)
            {
                screen.Hide();
            }
        }
        
        public void Show(string screenName) => ShowScreen(screenName);
    }
}
