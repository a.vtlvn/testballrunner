using UnityEngine;

namespace GameCore.UI
{
    public abstract class Screen : MonoBehaviour
    {
        public abstract string ScreenID { get; }

        public abstract void Show();
        
        public abstract void Hide();
    }
}
