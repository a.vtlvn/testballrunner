using GameCore.Coin;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.UI.Screens
{
    public class MainScreen : Screen
    {
        [SerializeField] private Text bestScoreText;
        [SerializeField] private string stringID;
        public override string ScreenID => stringID;


        public override void Show()
        {
            gameObject.SetActive(true);
            DisplayBestScore();
        }

        public override void Hide() => gameObject.SetActive(false);
        
        private void DisplayBestScore() => bestScoreText.text = "BestScore: " + CoinsService.Instance.GetBestScore();
    }
}
