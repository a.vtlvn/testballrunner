using GameCore.Coin;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.UI.Screens
{
    public class GameScreen : Screen
    {
        [SerializeField] private Text currentScoreText;
        [SerializeField] private string stringID;
        
        
        public override string ScreenID => stringID;

        private void OnEnable() => CoinsService.OnCoinsAdded += UpdateScore;

        private void OnDisable() => CoinsService.OnCoinsAdded -= UpdateScore;

        public override void Hide() => gameObject.SetActive(false);
        
        private void UpdateScore(int coins) => currentScoreText.text = "Your Score: " + coins;

        private void ResetScore() => currentScoreText.text = "Your Score: " + 0;
        
        public override void Show()
        {
            ResetScore();
            
            gameObject.SetActive(true);
         }
    }
}
