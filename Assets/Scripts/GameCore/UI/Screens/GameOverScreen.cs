using UnityEngine;

namespace GameCore.UI.Screens
{
    public class GameOverScreen : Screen
    {
        [SerializeField] private string stringID;
        public override string ScreenID => stringID;


        public override void Show()
        {
            gameObject.SetActive(true);
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
