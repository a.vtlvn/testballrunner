using System;
using GameCore.Services;
using UnityEngine;

namespace PlayerControllers
{
    public class BallMovement : MonoBehaviour
    {
        [SerializeField] private float characterSpeed = 5.0f;
        [SerializeField] private int currentTrack = 0;
        [SerializeField] private Vector3 ballStartPos;


        private void OnEnable() => GameService.OnGameOver += ResetBallPosition;
        
        private void OnDisable() => GameService.OnGameOver -= ResetBallPosition;

        private void Start() => ballStartPos = transform.position;

        private void Update()
        {
            if (!GameService.Instance.IsGameStarted)
                return;
            
#if UNITY_STANDALONE
             if (Input.GetKeyDown(KeyCode.A))
                SlidePLayer(--currentTrack);
            if (Input.GetKeyDown(KeyCode.D))
                SlidePLayer(++currentTrack);
#elif UNITY_ANDROID || UNITY_IOS
            if(Input.GetMouseButtonDown(0))
            {
                if (Input.mousePosition.x < Screen.width/2)
                {
                    SlidePLayer(--currentTrack);
                }
                else if (Input.mousePosition.x > Screen.width/2)
                {
                    SlidePLayer(++currentTrack);
                }
            }
#endif
            transform.position = new Vector3(currentTrack, transform.position.y, transform.position.z);
        }
        
        private void ResetBallPosition()
        {
            transform.position = ballStartPos;
            currentTrack = 0;
        }
        
        private void SlidePLayer(int toTrack)
        { 
            currentTrack += toTrack; 
            currentTrack = Mathf.Clamp(toTrack, -2, 2);
        }
    }
}
