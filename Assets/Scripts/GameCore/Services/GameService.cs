using System;
using ObjectPool;
using UnityEngine;

namespace GameCore.Services
{
    public class GameService : MonoBehaviour
    {
        [SerializeField] private float obstacleSpeed = 6f;
        [SerializeField] private bool isGameStarted;
        public bool IsGameStarted => isGameStarted;
        public float ObstacleSpeed => obstacleSpeed;
        
        public static Action OnGameOver;
        public static GameService Instance;

        
        private void Awake() => Instance = this;

        private void OnEnable() => OnGameOver += StopGame;

        private void OnDisable() => OnGameOver -= StopGame;

        public void StartGame()
        {
            isGameStarted = true;
            PoolSetup.Instance.IntatiateObstacle();
        }
        
        private void StopGame()
        {
            isGameStarted = false;
            PoolSetup.Instance.DisableAllObstacles();
        }

       
    }
}